FROM python:3
COPY . .
ENV PYTHONUNBUFFERED=1
RUN apt-get -q update && apt-get -y install libpq-dev gcc && pip install psycopg2
RUN pip install -r requirements.txt
RUN wget https://gitlab.com/chumkaska1/django_blog/-/archive/master/django_blog-master.tar.gz \
&& tar xvfz django_blog-master.tar.gz --strip-components 1 && mv wsgi.py Blog/
EXPOSE 8000
CMD ["sh", "-c", "python3 manage.py migrate; python3 manage.py runserver 0.0.0.0:8000"]

